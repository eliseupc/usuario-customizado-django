from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from .forms import UserCreationForm, UserChangeForm
from .models import UsuarioCustomizado


@admin.register(UsuarioCustomizado)
class UserAdmin(auth_admin.UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    model = UsuarioCustomizado
    fieldsets = auth_admin.UserAdmin.fieldsets + (
        ("Campos personalizados", {'fields': ('bio',)}),
    )
