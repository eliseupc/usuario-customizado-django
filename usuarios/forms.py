from django.contrib.auth import forms

from .models import UsuarioCustomizado


class UserChangeForm(forms.UserChangeForm):
    class Meta(forms.UserChangeForm.Meta):
        model = UsuarioCustomizado


class UserCreationForm(forms.UserCreationForm):
    class Meta(forms.UserCreationForm.Meta):
        model = UsuarioCustomizado

