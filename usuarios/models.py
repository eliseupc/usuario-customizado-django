from django.contrib.auth.models import AbstractUser
from django.db import models


class UsuarioCustomizado(AbstractUser):
    bio = models.TextField(blank=True)
    email = models.EmailField(max_length=500)

